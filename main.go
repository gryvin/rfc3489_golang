// RFC3489_GOlang project main.go
package main

import (
	"RFC3489_GOlang/sse"
	"RFC3489_GOlang/udp"
	"RFC3489_GOlang/utils"
	"flag"
	"fmt"
	"net"
	"net/http"
	//	"os"
	"strings"
	"time"
)

var (
	A1, P1, A2, P2 string //addresses & ports for UDP STUN binding
	SSE            string //Host:Port for SSE
)

func init() {
	//	RFC3489  8.1  Binding Requests
	//   A STUN server MUST be prepared to receive Binding Requests on four
	//   address/port combinations - (A1, P1), (A2, P1), (A1, P2), and (A2,
	//   P2).  (A1, P1) represent the primary address and port, and these are
	//   the ones obtained through the client discovery procedures below.
	//   Typically, P1 will be port 3478, the default STUN port.  A2 and P2
	//   are arbitrary.  A2 and P2 are advertised by the server through the

	flag.StringVar(&A1, "A1", "127.0.0.1", "IP for first STUN iface.")
	flag.StringVar(&P1, "P1", "3478", "Port for first STUN iface.")
	flag.StringVar(&A2, "A2", "", "IP for second STUN iface.")
	flag.StringVar(&P2, "P2", "2666", "Port for second STUN iface.")
	flag.StringVar(&SSE, "SSE", "localhost:8000", "HTTP Host:Port for SSE")
	flag.Parse()

	sa, err := net.InterfaceAddrs()
	utils.CheckErr(err)
	utils.CheckCond(!SystemHasIP(sa, A1), fmt.Sprintf("No system interface with address: %s", A1))
	utils.CheckCond(!SystemHasIP(sa, A2), fmt.Sprintf("No system interface with address: %s", A2))
}

func main() {
	// start logger over SSE
	logger := sse.NewLogger()
	logger.Start()

	http.Handle("/logmsg/", logger)
	go func() {
		for i := 0; ; i++ {
			logger.Log(fmt.Sprintf("%d - the time is %v", i, time.Now()))
			time.Sleep(5 * 1e9)
		}
	}()

	http.Handle("/", http.HandlerFunc(sse.MainPageHandler))
	utils.CheckErr(http.ListenAndServe(SSE, nil), "Invalid address for SSE")

	//start UDP server
	udp.NewUDPserver(logger)

}
