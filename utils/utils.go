package utils

import (
	"fmt"
	"os"
)

func CheckErr(er error, msg ...string) {
	if nil != er {
		out := ""
		if nil != msg {
			out = msg[0]
		}
		fmt.Println(fmt.Sprintf("FATAL: %s %s", out, er))
		os.Exit(1)
	}
}

func CheckCond(cond bool, msg string) {
	if cond {
		fmt.Println(fmt.Sprintf("FATAL: %s", msg))
		os.Exit(1)
	}
}

func SystemHasIP(s []net.Addr, e string) bool {
	for _, a := range s {
		if strings.HasPrefix(a.String(), e) {
			return true
		}
	}
	return false
}
