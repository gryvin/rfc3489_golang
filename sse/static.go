package sse

import (
	"fmt"
	"net/http"
)

func MainPageHandler(w http.ResponseWriter, r *http.Request) {
	const mainpage = `<!DOCTYPE html>
<html>
<body>
<h2>Log messages:</h2>
	<script type="text/javascript">
	    var source = new EventSource('/logmsg/');
	    source.onmessage = function(e) {
	        document.body.innerHTML += e.data + '<br>';
	    };
	</script>
</body>
</html>`
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, mainpage)
}
