package sse

import (
	"fmt"
	"net/http"
)

type Logger struct {
	clients        map[chan string]bool
	newClients     chan chan string
	defunctClients chan chan string
	messages       chan string
}

func NewLogger() *Logger {
	return &Logger{
		make(map[chan string]bool),
		make(chan (chan string)),
		make(chan (chan string)),
		make(chan string),
	}
}

func (b *Logger) Start() {
	go func() {
		for {
			select {
			case s := <-b.newClients:
				// new client attached - send messages to him
				b.clients[s] = true
			case s := <-b.defunctClients:
				// client detached
				delete(b.clients, s)
				close(s)
			case msg := <-b.messages:
				// push message for each attached client
				for s, _ := range b.clients {
					s <- msg
				}
			}
		}
	}()
}

func (b *Logger) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	f, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "SSE unsupported!", http.StatusInternalServerError)
		return
	}

	messageChan := make(chan string)
	b.newClients <- messageChan

	//HACKHACK! we are using CloseNotify in goroutine for reliability
	//  https://github.com/kljensen/golang-html5-sse-example/pull/1/files
	// we could loop endlessly because of
	// could not easily detect clients that dettach and the
	// server would continue to send them messages long after
	// they're gone due to the "keep-alive" header.  One of
	// the nifty aspects of SSE is that clients automatically
	// reconnect when they lose their connection.
	go func() {
		<-w.(http.CloseNotifier).CloseNotify()
		// Remove this client from attached when EventHandler exits
		b.defunctClients <- messageChan
	}()

	// Set the headers related to event streaming.
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	for {
		msg, open := <-messageChan
		if !open {
			break
		}
		fmt.Fprintf(w, "data: Message: %s\n\n", msg)
		f.Flush()
	}
}

func (b *Logger) Log(msg string) {
	b.messages <- msg
}
