package udp

import (
	"RFC3489_GOlang/sse"
	"RFC3489_GOlang/utils"
	"fmt"
	"net"
)

type logtype func(string)

type UDPserver struct {
	log         logtype
	Connections map[*net.UDPAddr]*net.UDPConn
	InQueue     chan struct { //queue for handling income datagrams
		buf     []byte
		src     *net.UDPAddr
		changed *net.UDPAddr
	}
}

func NewUDPserver(logger *sse.Logger) *UDPserver {
	return &UDPserver{
		logger.Log,
		make(map[*net.UDPAddr]*net.UDPConn),
		make(chan struct {
			buf     []byte
			src     *net.UDPAddr
			changed *net.UDPAddr
		}, 20),
	}
}

func udpReceiver(q *UDPserver, Alisten, Plisten, Achange, Pchange string) {
	addr, err := net.ResolveUDPAddr("udp", net.JoinHostPort(Alisten, Plisten))
	utils.CheckErr(err, fmt.Sprintf("Invalid address: %s:%s", Alisten, Plisten))
	conn, err := net.ListenUDP("udp", addr)
	utils.CheckErr(err, fmt.Sprintf("Network error: %s:%s", Alisten, Plisten))
	changed, _ := net.ResolveUDPAddr("udp4", net.JoinHostPort(Achange, Pchange))
	for {
		buf := make([]byte, 576)
		n, src, err := conn.ReadFromUDP(buf)
		if err != nil {
			q.log(fmt.Sprintf("Network error: %s at %s:%s", err, Alisten, Plisten))
		}
		q.InQueue <- struct {
			buf     []byte
			src     *net.UDPAddr
			changed *net.UDPAddr
		}{buf[:n], src, changed}
	}
}

func (q *UDPserver) Start(A1, P1, A2, P2 string) {
	go udpReceiver(q, A1, P1, A2, P2)
}
